terraform {
backend "s3" {
bucket="reef-terraform-state"
key="terraform/api_gateway/terraform_dev.tfstate"
region="us-west-2"
}
}
provider "aws" {
#  shared_credentials_file = "/Users/ashokkumar/.aws/credentials"
  profile                           =   "default"
  region                            =   "${var.aws_region}" 
}

module "api_gateway"{
    source  = "./modules/api_gateway"
    api_name = "${var.api_name}"
    swgfile = "${file(var.swgfile)}"
}
